[点击查看详细文档](http://zwyznswwy.com/note/v-directive/start/)

# vDirectives `vue3`

## 下载

```js
  npm install @skrey/v-directive
```

## 指令

- `v-debounce` 防抖指令
- `v-throttle` 节流指令
- `v-async` 同步指令
- `v-preview` 预览指令
- `v-focus` 聚焦指令

## 使用

### 全量使用

```js [main.js]
import vDirectives from '@skrey/v-directive'

// createApp

app.use(vDirectives)
```

### 部分使用

```js
import { customVDirective } from '@skrey/v-directive'

// createApp

customVDirective(app, {
  useDirectives: ['async', 'preview']
})
```

## 指定别名

```js
import { customVDirective } from '@skrey/v-directive'

// createApp

customVDirective(app, {
  useDirectives: ['async', 'preview'],
  alias: {
    preview: 'p', // 使用 v-p 代替 v-preview
    throttle: 't' // 使用 v-t 代替 v-throttle
  }
})
```

## 指令使用

### `v-debounce` 防抖指令

在一定时间间隔内重复发起调用，则取消前一次调用

```vue
<template>
  <div v-debounce="changeBg">点击切换背景</div>
</template>

<script lang="ts" setup>
const changeBg = () => {
  // 切换背景
}
</script>
```

几点注意：

- 默认事件 `click`
- 默认防抖时间 `300ms`

更复杂的使用（修饰符和 `vue` 保持一致）

```vue
<div v-debounce:contextmenu-500.prevent="changeBg">点击切换背景</div>
// 表示绑定 contextmenu 事件， 防抖时长500ms并且阻止默认事件

<div v-debounce:click-resize-.prevent.stop="changeBg">点击切换背景</div>
// 表示绑定 click 事件和 resize 事件，使用默认防抖时长，并且阻止冒泡和默认事件

<div v-debounce:-1000.prevent.stop="changeBg">点击切换背景</div>
// 表示绑定默认的 click 事件，修改防抖时长为1000ms，并且阻止冒泡和默认事件
```

### `v-throttle` 节流指令

以一定的频率发起调用

```vue
<template>
  <div v-throttle="update">点击更新</div>
</template>

<script lang="ts" setup>
const update = () => {
  // 更新
}
</script>
```

几点注意：

- 默认事件 `click`
- 默认节流时间 `300ms`

更复杂的使用（修饰符和 `vue` 保持一致）

```vue
<div v-throttle:contextmenu-500.prevent="update">点击更新</div>
// 表示绑定 contextmenu 事件， 节流时长500ms并且阻止默认事件

<div v-throttle:click-resize-.prevent.stop="update">点击更新</div>
// 表示绑定 click 事件和 resize 事件，使用默认节流时长，并且阻止冒泡和默认事件

<div v-throttle:-1000.prevent.stop="update">点击更新</div>
// 表示绑定默认的 click 事件，修改节流时长为1000ms，并且阻止冒泡和默认事件
```

### `v-async` 同步指令

当调用返回结果后（无论结果如何）才能发起下一次调用

```vue
<template>
  <div v-async="getData">点击获取数据</div>
</template>

<script lang="ts" setup>
const getData = () => {
  // 获取数据
}
</script>
```

几点注意：

- 默认事件 `click`
- 默认休眠时间 `0`，可以设置调用成功（无论结果如何）之后多久可以再次发起调用

更复杂的使用（修饰符和 `vue` 保持一致）

```vue
<div v-async:contextmenu-500.prevent="getData">点击获取数据</div>
// 表示绑定 contextmenu 事件， 休眠时长500ms并且阻止默认事件

<div v-async:click-resize-.prevent.stop="getData">点击获取数据</div>
// 表示绑定 click 事件和 resize 事件，使用默认休眠时长，并且阻止冒泡和默认事件

<div v-async:-1000.prevent.stop="getData">点击获取数据</div>
// 表示绑定默认的 click 事件，修改休眠时长为1000ms，并且阻止冒泡和默认事件
```

### `v-preview` 预览指令

用于图片预览

```vue
<template>
  <img
    class="w-20 h-20 rounded-lg"
    v-preview
    src="http://zwyznswwy.com:3000/public/website/home/images/bg2.webp"
  />
</template>
```

更复杂的使用:

```vue
<template>
  <img
    v-preview="{
      background: 'red',
      margin: 24
    }"
    src="http://zwyznswwy.com:3000/public/website/home/images/bg2.webp"
  />
</template>
// binding.value 为 preview 的配置选项
```

可设置的配置有：

```js
 {
  margin: 24, // 缩放图像外边距
  background: '#BADA55', // 缩放图像的背景
  scrollOffset: 0, // 滚动以关闭缩放的距离
  container: '#zoom-container', // 指定缩放容器
  template: '#zoom-template', // 指定缩放模板
  useInstance: (instance:Zoom) => {}, // useInstance 帮助我们拿到实例做进一步的处理
}
```

默认配置

```js
{
  margin: 24,
  background: 'rgba(0,0,0,.3)',
  scrollOffset: 0,
  useInstance: () => {}
}
```

实例方法(部分)

```js
instance.open() // 在调用 open 方法时立即触发。
instance.opened() // 在缩放完成动画后触发。
instance.close() // 在调用 close 方法时立即触发。
instance.toggle() // 打开时关闭，关闭时打开。
instance.closed() // 在缩小完成动画后触发。
instance.detach() // 在调用 detach 方法时触发。
instance.update() // 在调用 update 方法时触发。

instance.on('open', function (event) {
  console.log('Image is zoomed in', event)
})
```

> [点击前往 `medium-zoom`](https://www.npmjs.com/package/medium-zoom)

### `v-focus` 聚焦指令

让页面的输入框自动聚焦

- 对于 `input` 下列属性将起作用
  'email',
  'number',
  'password',
  'search',
  'tel',
  'text',
  'url',
  'datetime'
- 对于 `textarea` 没有特殊要求

修饰符

- `global` 开启全局查找
- `autoFind` 自动查找

**注意**
**类选择器 `.` 使用 `-v-`代替，例如 `v-focus:-v-className`**

```vue
// 简单使用
<template><input v-focus /> // 聚焦</template>

// 指定选择器 -- 在 input 或者 textarea 上 则不起作用
<template><input v-focus:#text /> // 聚焦</template>

// 指定选择器 -- 将寻找id = 'text' 的元素并使其聚焦，如果选择的为不可聚焦元素，则无效果,
开启自动查找可以聚焦符合条件的第一个
<template>
  <div v-focus:#text><input /> // 无效果</div>
</template>

// 指定选择器 -- 将全局寻找id = 'text' 的元素并使其聚焦，如果选择的为不可聚焦元素，则无效果,
开启自动查找可以聚焦符合条件的第一个
<template>
  <input id="text" /> // 聚焦
  <div v-focus:#text.global>
    <input />
  </div>
</template>

// 配合 autoFind
<template>
  <input id="text1" />
  <div v-focus:#text.global.autoFind><input /> // 聚焦</div>
</template>
```

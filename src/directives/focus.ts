import message from '@/message'
import { canFocusType, willDeleteType, canFocusEl } from '@/types/focus'

// 聚焦
const focusDirective = {
  mounted(el: HTMLElement, binding: any) {
    // 获取标签名称
    let focusEl: HTMLElement | null = el
    const selector = binding.arg ? binding.arg.replaceAll('-v-', '.') : ''

    let tagName = focusEl ? focusEl.tagName.toLowerCase() : ''

    // 修饰符
    const { global, autoFind } = binding.modifiers
    /*
      存在选择器，并且没有将指令直接绑定在 canFocusEl 元素上（这些元素不再存在子元素） 
    */
    if (selector) {
      const bindInCanFocusEl = canFocusEl.includes(tagName)
      if (global) {
        focusEl = document.querySelector(selector)
      } else {
        focusEl = focusEl!.querySelector(selector)
      }
      if (!focusEl && bindInCanFocusEl) {
        focusEl = el
      }
    }
    tagName = focusEl ? focusEl.tagName.toLowerCase() : ''
    if (autoFind && !canFocusEl.includes(tagName)) {
      // 不符合 查找第一个
      focusEl = el.querySelector('input, textarea')
    }
    tagName = focusEl ? focusEl.tagName.toLowerCase() : ''
    if (!focusEl) {
      return message.warning('不存在可绑定的<input>或者<textarea>标签')
    }
    // 获取type
    if (tagName === 'input') {
      const focusElType = focusEl.getAttribute('type') || ''
      if (!canFocusType.includes(focusElType)) {
        return message.warning(`不可绑定属性为[${focusElType}]的<input>标签`)
      }
      if (willDeleteType.includes(focusElType)) {
        message.warning(`属性为[${focusElType}]已废弃`)
      }
    }
    focusEl.focus()
  }
}

export default focusDirective

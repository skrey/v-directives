import type { ThrottleDirective, MapItem } from '@/types/throttle'

// 节流
const throttleEls = new Map<HTMLElement, MapItem>()
const throttleDirective: ThrottleDirective = {
  mounted(el: HTMLElement, binding: any) {
    // 已经监听过了
    if (throttleEls.has(el)) return
    const arg = binding.arg || ''
    const args: Array<string> = arg ? arg.split('-').filter((arg: string) => arg) : []
    const time: number = !isNaN(Number(args[args.length - 1])) ? Number(args.pop()) : 300
    const events: Array<string> = args
    // 时长 - 默认 300
    const throttleTime = time
    // 响应函数
    const responseFunction = binding.value
    // 修饰符
    const { firstPass, stop, prevent, capture, once, passive } = binding.modifiers
    // 包装响应函数
    const callback = (e: Event) => {
      stop && e.stopPropagation()
      prevent && e.preventDefault()
      if (throttleEls.get(el)?.timer) {
        return
      }
      // 首次不推迟
      if (firstPass) {
        responseFunction()
        throttleEls.get(el)!.timer = setTimeout(() => {
          clearTimeout(throttleEls.get(el)!.timer as NodeJS.Timeout)
          throttleEls.get(el)!.timer = 0
        }, throttleTime)
      } else {
        // 首次推迟
        throttleEls.get(el)!.timer = setTimeout(() => {
          responseFunction()
          clearTimeout(throttleEls.get(el)!.timer as NodeJS.Timeout)
          throttleEls.get(el)!.timer = 0
        }, throttleTime)
      }
    }
    // 添加事件 - 默认click
    const needEvents = events.length ? events : ['click']
    needEvents.forEach(needEvent => {
      el.addEventListener(needEvent, callback, {
        capture, // === useCapture
        once, // 是否设置单次监听,if true，会在调用后自动销毁listener
        passive
      })
    })
    // 添加标记
    throttleEls.set(el, {
      timer: 0,
      callback,
      needEvents
    })
  },
  beforeUnmount(el: HTMLElement) {
    throttleEls.get(el)?.needEvents.forEach(needEvent => {
      el.removeEventListener(needEvent, throttleEls.get(el)?.callback || (() => {}))
    })
  }
}

export default throttleDirective

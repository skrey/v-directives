import { LoadingDirective, MapItem } from '@/types/loading'

// const reolvePromise = (reolveTime: number): Promise<unknown> => {
//   // 指定时间后返回状态成功的promise
//   return new Promise(resolve => {
//     setTimeout(() => {
//       resolve(`在${reolveTime}ms后返回成功Promise`)
//     }, reolveTime)
//   })
// }

// const rejectPromise = (rejectTime: number): Promise<unknown> => {
//   // 指定时间后返回状态失败的promise
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       reject(new Error(`在${rejectTime}ms后返回失败Promise`))
//     }, rejectTime)
//   })
// }

// funcs
// 创建loading-box
const createLoadingBox = (style: string): HTMLElement => {
  const loadingBox = document.createElement('div')
  loadingBox.classList.add('l-loading__box')
  const loader = document.createElement('div')
  if (!/^(style)[1-9]$/.test(style)) {
    loader.innerHTML = style
  } else {
    loader.classList.add(style)
  }
  loadingBox.append(loader)
  return loadingBox
}

// 加载状态
const loadingEls = new Map<HTMLElement, MapItem>()
const loadingDirective: LoadingDirective = {
  mounted(el: HTMLElement, binding: any) {
    // 监听过该元素
    if (loadingEls.has(el)) return
    // 加载状态
    const loading = binding.value
    // 样式选择
    const style = binding.arg || 'style1'
    // relative
    const computedStyle = getComputedStyle(el, null)
    if (computedStyle.position === 'static') {
      el.style.position = 'relative'
    }

    if (loading) {
      const loadingBox = createLoadingBox(style)
      loadingEls.set(el, {
        loading,
        loadingBox
      })
      el.appendChild(loadingBox)
    }
  },
  updated(el, binding: any) {
    // 加载状态
    const loading = binding.value

    if (loading) {
      if (!loadingEls.get(el)?.loadingBox) {
        // 样式选择
        const style = binding.arg || 'style1'
        const loadingBox = createLoadingBox(style)
        loadingEls.set(el, {
          loading,
          loadingBox
        })
        el.appendChild(loadingBox)
      }
    } else {
      if (loadingEls.get(el)?.loadingBox) {
        el.removeChild(loadingEls.get(el)?.loadingBox)
        loadingEls.set(el, {
          loading,
          loadingBox: null
        })
      }
    }
  }
}

export default loadingDirective

import { AsyncDirective, MapItem } from '@/types/async'

// 节流
const asyncEls = new Map<HTMLElement, MapItem>()
const asyncDirective: AsyncDirective = {
  mounted(el: HTMLElement, binding: any) {
    // 已经监听过了
    if (asyncEls.has(el)) return
    const arg = binding.arg || ''
    const args: Array<string> = arg ? arg.split('-').filter((arg: string) => arg) : []
    const sleep: number = !isNaN(Number(args[args.length - 1])) ? Number(args.pop()) : 0
    const events: Array<string> = args
    // 休息时长 - 默认 0
    const sleepTime = sleep
    // 响应函数
    const responseFunction = binding.value
    // 修饰符
    const { stop, prevent, capture, once, passive } = binding.modifiers
    // 包装响应函数
    const callback = async (e: Event) => {
      stop && e.stopPropagation()
      prevent && e.preventDefault()
      if (asyncEls.get(el)?.loading) {
        return
      }
      asyncEls.get(el)!.loading = true
      await responseFunction()
      if (sleepTime) {
        setTimeout(() => {
          asyncEls.get(el)!.loading = false
        }, sleepTime)
      } else {
        asyncEls.get(el)!.loading = false
      }
    }
    // 添加事件 - 默认click
    const needEvents = events.length ? events : ['click']
    needEvents.forEach(needEvent => {
      el.addEventListener(needEvent, callback, {
        capture, // === useCapture
        once, // 是否设置单次监听,if true，会在调用后自动销毁listener
        passive
      })
    })
    // 添加标记
    asyncEls.set(el, {
      loading: false,
      callback,
      needEvents
    })
  },
  beforeUnmount(el: HTMLElement) {
    asyncEls.get(el)?.needEvents.forEach(needEvent => {
      el.removeEventListener(needEvent, asyncEls.get(el)?.callback || (() => {}))
    })
  }
}

export default asyncDirective

import { CopyDirective, canEvent, BindingValue, MapItem } from '@/types/copy'
import message from '@/message'
// 拷贝
const copyEls = new Map<HTMLElement, MapItem>()
const copyDirective: CopyDirective = {
  mounted(el: HTMLElement, binding: any) {
    if (copyEls.has(el)) return
    const arg = binding.arg || ''
    const args: Array<string> = arg ? arg.split('-').filter((arg: string) => arg) : []
    const events: Array<string> = args.filter(event => canEvent.includes(event))
    const needEvents = events.length ? events : ['click']
    // 修饰符
    const { stop, prevent, capture, once, passive } = binding.modifiers
    // value
    const bindValue: BindingValue | string = binding.value || ''
    const bindValueType = Object.prototype.toString.call(bindValue)
    let copyText = ''
    let success = (...args: any) => {}
    let error = (...args: any) => {}
    if (bindValueType === '[object String]') {
      copyText = bindValue as string
    } else if (bindValueType === '[object Object]') {
      copyText = (bindValue as BindingValue)?.copyText || ''
      if ((bindValue as BindingValue).success) {
        success = (bindValue as BindingValue).success!
      }
      if ((bindValue as BindingValue).error) {
        error = (bindValue as BindingValue).error!
      }
    } else {
      return message.warning('binding 只能绑定字符串或者对象')
    }
    // 响应函数
    const callback = (e: Event) => {
      stop && e.stopPropagation()
      prevent && e.preventDefault()
      if (isSecureContext) {
        if (typeof ClipboardItem && navigator.clipboard.write) {
          const text = new ClipboardItem({
            'text/plain': new Blob([copyText], { type: 'text/plain' })
          })
  
          navigator.clipboard.write([text]).then(
            () => {
              success(e, copyText)
            },
            err => {
              error(err, copyText)
            }
          )
        } else {
          navigator.clipboard
            .writeText(copyText)
            .then(() => {
              success(e, copyText)
            })
            .catch(err => {
              error(err, copyText)
            })
        } 
      }else {
        try {
          const copyInput = document.createElement('input');
          copyInput.style.position = "absolute";
          copyInput.style.opacity = '0';
          copyInput.style.left = "-999999px";
          copyInput.style.top = "-999999px";
          copyInput.value = copyText;
          document.body.appendChild(copyInput);
          copyInput.select();
          document.execCommand('copy');
          document.body.removeChild(copyInput);
          success(e, copyText)
        } catch(err) {
          error(err, copyText)
        }
      }
    }

    // 绑定事件
    needEvents.forEach(needEvent => {
      el.addEventListener(needEvent, callback, {
        capture, // === useCapture
        once, // 是否设置单次监听,if true，会在调用后自动销毁listener
        passive
      })
    })

    copyEls.set(el, {
      callback,
      needEvents
    })
  },
  beforeUnmount(el: HTMLElement) {
    copyEls.get(el)?.needEvents.forEach(needEvent => {
      el.removeEventListener(needEvent, copyEls.get(el)?.callback || (() => {}))
    })
  }
}

export default copyDirective

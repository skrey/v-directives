import { LazyImageDirective, BindingValue, MapItem } from '@/types/lazyImage'
import config from '@/config'
import message from '@/message'

// 懒加载
const lazyImageEls = new Map<HTMLElement, MapItem>()
const copyDirective: LazyImageDirective = {
  mounted(el: HTMLElement, binding: any) {
    if (lazyImageEls.has(el)) return
    const arg = binding.arg || 'image'
    let bindingValue: BindingValue = binding.value
    const bindingValueType = Object.prototype.toString.call(bindingValue)
    const { once } = binding.modifiers
    let tagName = el.tagName.toLowerCase() || ''
    let src = ''
    if (bindingValueType === '[object String]') {
      src = bindingValue as unknown as string
    } else if (bindingValueType === '[object Object]') {
      src = bindingValue.src
      bindingValue = { ...config.lazyImage, ...bindingValue }
    } else {
      return message.warning('binding 只能绑定字符串或者对象')
    }
    // setSrc
    const setSrc = (rSrc: string) => {
      if (arg === 'image') {
        ;(el as HTMLImageElement).src = rSrc
      } else if (arg === 'background') {
        el.style.background = `url(${rSrc}) 100%/cover`
      }
    }

    // 替换src
    const replaceSrc = () => {
      setSrc(bindingValue.loadingSrc!)
      const startTime = performance.now()
      let time = bindingValue.loadingTime || 0
      time = isNaN(Number(time)) ? 500 : Number(time)
      const img = new Image()
      img.src = src
      img.onload = () => {
        const endTime = performance.now()
        let useTime = endTime - startTime
        const loadingTime = useTime > time ? 0 : time - useTime
        setTimeout(() => {
          setSrc(src)
        }, loadingTime)
      }
    }

    // 回调
    const callback = (entries: Array<IntersectionObserverEntry>) => {
      const isIntersecting = entries[0].isIntersecting
      if (isIntersecting) {
        if (once) {
          lazyImageEls.get(el)!.intersectionObserver.unobserve(el)
        }
        if (bindingValue.loadingSrc) {
          replaceSrc()
        } else {
          setSrc(src)
        }
        bindingValue.show && bindingValue.show()
      } else {
        bindingValue.hide && bindingValue.hide()
      }
    }
    // value
    if (arg !== 'image' && arg !== 'background') {
      return message.warning('仅支持 image 和 background')
    } else if (arg === 'image' && tagName !== 'img') {
      return message.warning('image 仅配合 img 标签使用')
    } else {
      const intersectionObserver = new IntersectionObserver(callback, bindingValue.options)
      intersectionObserver.observe(el)
      lazyImageEls.set(el, {
        intersectionObserver,
        callback
      })
    }
  },
  beforeUnmount(el: HTMLElement) {
    lazyImageEls.get(el)!.intersectionObserver.unobserve(el)
  }
}

export default copyDirective

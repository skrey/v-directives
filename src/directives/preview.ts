import message from '@/message'
import mediumZoom from 'medium-zoom'
import type { VDZoomOptions } from '@/types/preview'
import VDConfig from '@/config'

// 预览
const preViewDirective = {
  mounted(el: HTMLElement, binding: any) {
    // 获取标签名称
    const tagName = el.tagName
    if (tagName.toLowerCase() !== 'img') {
      return message.warning('不能用于绑定非<img>标签')
    }
    const config: VDZoomOptions = binding.value
      ? { ...VDConfig.preview, ...binding.value }
      : VDConfig.preview

    const instance = mediumZoom(el, config)
    if (config.useInstance) {
      config.useInstance(instance)
    }
  }
}

export default preViewDirective

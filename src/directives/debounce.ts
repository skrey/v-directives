import { DebounceDirective, MapItem } from '@/types/debounce'

// 防抖
const debounceEls = new Map<HTMLElement, MapItem>()
const debounceDirective: DebounceDirective = {
  mounted(el: HTMLElement, binding: any) {
    // 已经监听过了
    if (debounceEls.has(el)) return
    const arg = binding.arg || ''
    const args: Array<string> = arg ? arg.split('-').filter((arg: string) => arg) : []
    const time: number = !isNaN(Number(args[args.length - 1])) ? Number(args.pop()) : 300
    const events: Array<string> = args

    // 时长 - 默认 300
    const debounceTime = time
    // 响应函数
    const responseFunction = binding.value

    // 修饰符
    const { stop, prevent, capture, once, passive } = binding.modifiers
    // 包装响应函数
    const callback = (e: Event) => {
      stop && e.stopPropagation()
      prevent && e.preventDefault()
      debounceEls.get(el)?.timer && clearTimeout(debounceEls.get(el)!.timer as NodeJS.Timeout)
      debounceEls.get(el)!.timer = setTimeout(() => {
        responseFunction(e)
      }, debounceTime)
    }

    // 添加事件
    const needEvents = events.length ? events : ['click']
    needEvents.forEach(needEvent => {
      el.addEventListener(needEvent, callback, {
        capture, // === useCapture
        once, // 是否设置单次监听,if true，会在调用后自动销毁listener
        passive
      })
    })
    // 添加标记
    debounceEls.set(el, {
      timer: 0,
      callback,
      needEvents
    })
  },
  beforeUnmount(el: HTMLElement) {
    debounceEls.get(el)?.needEvents.forEach(needEvent => {
      el.removeEventListener(needEvent, debounceEls.get(el)?.callback || (() => {}))
    })
  }
}

export default debounceDirective

import type { App } from 'vue'
import { createApp } from 'vue'
import message from '@/message'
import { Directives, UseDirectivesObj, Config } from './types'
import config from './config'
import debounce from '@/directives/debounce'
import throttle from '@/directives/throttle'
import async from '@/directives/async'
import preview from '@/directives/preview'
import focus from '@/directives/focus'
import copy from '@/directives/copy'
import lazyImage from '@/directives/lazyImage'
import loading from '@/directives/loading'
import { VDConfig } from './types/config'
import { VDZoomOptions } from './types/preview'
import { LazyImageOptions } from './types/lazyImage'
import './style/loading.css'

// 指令
const directives: Directives = {
  debounce,
  throttle,
  async,
  preview,
  focus,
  copy,
  lazyImage,
  loading
}

// 注入函数
const useD = (app: App, directives: UseDirectivesObj) => {
  Object.keys(directives).forEach(key => {
    app.directive(key, directives[key as keyof UseDirectivesObj] as any)
  })
}

// 合并config
const mergeConfig = (customConfig: VDConfig) => {
  for (let key in customConfig) {
    if (config.hasOwnProperty(key)) {
      if (key === 'preview') {
        config[key] = {
          ...config[key as keyof VDConfig],
          ...customConfig[key as keyof VDConfig]
        } as VDZoomOptions
      } else if (key === 'lazyImage') {
        config[key] = {
          ...config[key as keyof VDConfig],
          ...customConfig[key as keyof VDConfig]
        } as LazyImageOptions
      }
    }
  }
}

// 选择
export const customVDirective = (
  app: App, // app
  config: Config = {}
) => {
  // if (!(app instanceof createApp)) {
  //   return message.error('参数app 应为 vue 的实例')
  // } else if (Array.isArray(config.useDirectives)) {
  //   return message.error('参数useDirectives 应为字符串数组')
  // } else if () {

  // } else if() {

  // } else {

  // }
  // 合并config
  mergeConfig(config.config || {})
  const useDirectivesObj: UseDirectivesObj = {}
  const useDirectives = config.useDirectives || Object.keys(directives)
  const alias = config.alias || {}
  useDirectives.forEach(key => {
    if (alias[key as keyof Directives]) {
      useDirectivesObj[alias[key as keyof Directives] as string] =
        directives[key as keyof Directives]
    } else {
      useDirectivesObj[key as keyof Directives] = directives[key as keyof Directives]
    }
  })
  useD(app, useDirectivesObj)
}

// 全量注入
const vDirectives = {
  install(app: App) {
    useD(app, directives)
  }
}

export default vDirectives

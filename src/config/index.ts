import { VDConfig } from '@/types/config'

const config: VDConfig = {
  preview: {
    margin: 24,
    background: 'rgba(0,0,0,.3)',
    scrollOffset: 0,
    useInstance: () => {}
  },
  lazyImage: {
    loadingTime: 500,
    show: () => {},
    hide: () => {},
    options: {
      root: null,
      rootMargin: '0px 0px 0px 0px',
      threshold: 0.0
    }
  }
}

export default config

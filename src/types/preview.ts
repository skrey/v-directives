import type { ZoomOptions } from 'medium-zoom'

export type VDZoomOptions = ZoomOptions & {
  useInstance: (...args: any) => void
}

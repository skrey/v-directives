import type { ObjectDirective } from 'vue'

export type LazyImageDirective = ObjectDirective

export interface MapItem {
  intersectionObserver: IntersectionObserver
  callback: (...args: any) => void
}

export type IntersectionObserverOptions = {
  root: Element | null
  rootMargin: string
  threshold: number
}

export type ExtraOptions = {
  loadingSrc?: string
  loadingTime?: number
  show: (...args: any) => void
  hide: (...args: any) => void
}

export type BindingValue = ExtraOptions & {
  src: string
  options?: IntersectionObserverOptions
}

export type LazyImageOptions = ExtraOptions & {
  options: IntersectionObserverOptions
}

import type { VDZoomOptions } from './preview'
import type { LazyImageOptions } from './lazyImage'

export interface VDConfig {
  preview?: VDZoomOptions
  lazyImage?: LazyImageOptions
}

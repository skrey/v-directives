import { VDConfig } from './config'

export type Directive =
  | 'debounce'
  | 'throttle'
  | 'async'
  | 'preview'
  | 'focus'
  | 'copy'
  | 'lazyImage'
  | 'loading'

export type Directives = {
  [key in Directive]: Object
}

export type DirectivesNames = {
  [key in Directive]: key
}

export type UseDirectivesObj = {
  [key in string]?: Object
}

export type UseDirectives = Array<Directive>

export type UseDirectivesAlias = {
  [key in Directive]?: string
}

export type Config = {
  useDirectives?: UseDirectives // 注册指令
  alias?: UseDirectivesAlias // 指令别名
  config?: VDConfig
}

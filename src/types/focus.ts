// 可聚焦的元素
export const canFocusEl = ['input', 'textarea']

// 可用的 type 属性
export const canFocusType = [
  'email',
  'number',
  'password',
  'search',
  'tel',
  'text',
  'url',
  'datetime'
]

// mdn 废弃的属性
export const willDeleteType = ['datetime']

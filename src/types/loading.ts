import type { ObjectDirective } from 'vue'

export type LoadingDirective = ObjectDirective

export interface MapItem {
  loading: boolean,
  loadingBox: HTMLElement | null
}
import type { ObjectDirective } from 'vue'

export const canEvent = ['click', 'contextmenu']

export type CopyDirective = ObjectDirective

export interface MapItem {
  needEvents: Array<string>
  callback: (...args: any) => void
}

export type BindingValue = {
  copyText: string
  success?: (...args: any[]) => any
  error?: (...args: any[]) => any
}

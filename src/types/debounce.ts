import type { ObjectDirective } from 'vue'

export type DebounceDirective = ObjectDirective

export interface MapItem {
  timer: NodeJS.Timer | 0 | undefined
  callback: (...args: any) => void
  needEvents: Array<string>
}

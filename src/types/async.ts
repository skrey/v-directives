import type { ObjectDirective } from 'vue'

export type AsyncDirective = ObjectDirective
export interface MapItem {
  loading: boolean
  needEvents: Array<string>
  callback: (...args: any) => void
}

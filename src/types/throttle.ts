import type { ObjectDirective } from 'vue'

export type ThrottleDirective = ObjectDirective

export interface MapItem {
  timer: NodeJS.Timeout | 0 | undefined
  callback: (...args: any) => void
  needEvents: Array<string>
}

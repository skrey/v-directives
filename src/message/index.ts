export default {
  warning(message: string) {
    console.warn('[vDirectives]: ' + message)
  },
  error(message: string) {
    console.error('[vDirectives]: ' + message)
  },
  info(message: string) {
    console.info('[vDirectives]: ' + message)
  }
}

import resolve from '@rollup/plugin-node-resolve'
import typescript from '@rollup/plugin-typescript'
import commonjs from '@rollup/plugin-commonjs'
import { terser } from 'rollup-plugin-terser'
import filesize from 'rollup-plugin-filesize'
import alias from '@rollup/plugin-alias'
import dynamicImportVars from '@rollup/plugin-dynamic-import-vars'
import postcss from 'rollup-plugin-postcss'

export default {
  input: './src/index.ts',
  output: [
    {
      dir: 'dist',
      format: 'cjs',
      entryFileNames: '[name].cjs.js',
      exports: 'named'
    },
    {
      dir: 'dist',
      format: 'esm',
      entryFileNames: '[name].esm.js',
      exports: 'named'
    }
  ],
  plugins: [
    postcss({
      extensions: ['.css']
    }),
    alias({
      entries: [{ find: '@', replacement: 'src' }]
    }),
    dynamicImportVars(),
    resolve(),
    commonjs(),
    typescript(),
    terser(),
    filesize()
  ]
}
